from django import forms

class Jadwal_Form(forms.Form):
	
	error_messages ={
	'required': 'This field is required',
	'invalid' : 'Harap memasukan input yang sesuai',
	}
	
	attrs = {
	'size' : 100,
	'class': 'form-control'
	}
	
	namaKegiatan = forms.CharField(label='Nama Kegiatan', required=True, widget=forms.TextInput(attrs=attrs))
	hari = forms.CharField(label='Hari', required=True, widget=forms.TextInput(attrs=attrs))
	tanggal = forms.DateField(label='Tanggal', required=True, widget=forms.DateInput(attrs={'type':'date'}))
	jam = forms.TimeField(label='Jam', required=True, widget=forms.TimeInput(attrs={'type':'time'}))
	tempat = forms.CharField(label='Tempat', required=True, widget=forms.TextInput(attrs=attrs))
	