from django.db import models

# Create your models here.

class JadwalKegiatan (models.Model):
	namaKegiatan = models.CharField(max_length=100)
	hari = models.CharField(max_length=100)
	tanggal = models.DateField()
	jam = models.TimeField()
	tempat = models.CharField(max_length=100)
	
	def __str__(self):
		return self.namaKegiatan