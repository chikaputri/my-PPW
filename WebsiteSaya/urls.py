from django.conf.urls import url
from .views import home
from .views import educated
from .views import about
from .views import contact
from .views import formKegiatan
from .views import jadwalHasil

urlpatterns = [
	url(r'^$', home, name='home'),
	url(r'^about/', about, name='about'),
	url(r'^educated/', educated, name='educated'),
	url(r'^contact', contact, name='contact'),
	url(r'^home', home, name='home'),
	url(r'^formKegiatan/', formKegiatan, name ='formKegiatan'),
	url(r'^jadwalHasil/', jadwalHasil, name ='jadwalHasil'),
]