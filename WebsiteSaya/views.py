from django.shortcuts import render
from .forms import Jadwal_Form
from .models import JadwalKegiatan
from django.http import HttpResponseRedirect
import pytz
import json

# Create your views here.

response = {'author' : 'ChikaPutri'}

def home(request):
	return render(request, 'home.html')
	
	
def about(request):
	return render(request, 'about.html')
	
	
def educated(request):
	return render(request, 'educated.html')
	
	
def contact(request):
	return render(request, 'contact.html')
	
	
def formKegiatan(request):
	form = Jadwal_Form(request.POST or None)
	if (request.method == 'POST' and form.is_valid()):
			response['namaKegiatan'] = request.POST['namaKegiatan']
			response['hari'] = request.POST['hari']
			response['tanggal'] = request.POST['tanggal']
			response['jam'] = request.POST['jam']
			response['tempat'] = request.POST['tempat']
			
			message = JadwalKegiatan(namaKegiatan=response['namaKegiatan'], 
				hari=response['hari'], tanggal=response['tanggal'], 
				jam=response['jam'], tempat=response['tempat'])
			message.save()
	
	else:
		print("Salah Input!")
	
	response['form_message'] = form
	html = 'formKegiatan.html'
	return render(request, html, response)
	

def jadwalHasil(request):
	result = JadwalKegiatan.objects.all()
	response['form_result'] = results
	return render(request, 'formHasil.html', response)
	
def deleteJadwal(request):
	JadwalKegiatan.objects.all().delete()
	return HttpResponseRedirect("../jadwalHasil")

	
	
	
	
	
	
	
	
	
	
	